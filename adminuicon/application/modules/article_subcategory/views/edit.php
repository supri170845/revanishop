<form method="post" action="<?=base_url();?>article_subcategory/update_proses" enctype="multipart/form-data" >
<div style="margin-bottom: 5px;text-align: right;">
    <input type="submit" class="btn btn-primary" value="Save" /> <input type="button" onclick="window.location.replace('<?php echo base_url(); ?>article_subcategory/search/<?=$posisi;?>');" class="btn btn-danger" value="Cancel" />
</div>
<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title"> Update Article Sub Category </h3>
    </div>
    <div class="panel-body" id="panelx">
        <div class="form-group">
                    <label>ID</label><input type="hidden" name="posisi" value="<?=$posisi;?>"/>
                    <input class="form-control" type="text" name="id" value="<?=$list_detail->id;?>" readonly/>
                </div>
        <div class="form-group">
            <label>Category</label>
                <select class="combobox" style="width: 100%;font-size: 10px;" required name="category">
                    <option value="">Set Category</option>\
                    <?php
                        foreach($list_category as $category){
                            if($category->id==$list_detail->id_category){
                                $select="selected";
                            }else{
                                $select="";
                            }
                            echo"<option value='$category->id' $select>$category->article_category_name</option>";
                        }
                    ?>
                </select>
            </div>
        <div class="row" style="font-size: 12px;">
            <div class="col-lg-12">
                <div class="form-group">
                    <label>Sub Category Name</label>
                    <input type="text" class="form-control" name="subcategory_name" value="<?=$list_detail->article_subcategory_name;?>"  required/>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" id="editor1" name="category_description"><?=$list_detail->article_subcategory_description;?></textarea>
                </div>
                <div class="form-group">
                    <label>Set Status</label>
                    <select class="combobox" style="width: 100%;font-size: 10px;" name="status" required>
                        <?php
                            if($list_detail->status == "Y"){
                                $a="selected";
                            }else{
                                $a="";
                            }
                            
                            if($list_detail->status == "N"){
                                $b="selected";
                            }else{
                                $b="";
                            }
                        ?>
                        <option value="Y" <?=$a;?>/>Active</option>
                        <option value="N" <?=$b;?>/>Not Active</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<?php $this->load->view('combobox_autocomplete');?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<?php $this->load->view('ckeditor');?>
<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'editor1',
    {
        filebrowserBrowseUrl: '<?php echo base_url();?>assets/ckfinder/ckfinder.html',
        filebrowserUploadUrl: '<?php echo base_url();?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
    });
</script>