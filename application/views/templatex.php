﻿<!DOCTYPE HTML>
<?php $this->load->view("fn_lib/lib_function");?>
<html>
<head>
    <meta charset="utf-8">
    <title>REVANISHOP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Html5TemplatesDreamweaver.com">
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> <!-- Remove this Robots Meta Tag, to allow indexing of site -->

    <link href="<?php echo base_url();?>themes/scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>themes/scripts/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->
    <link href="<?php echo base_url();?>themes/scripts/icons/general/stylesheets/general_foundicons.css" media="screen" rel="stylesheet" type="text/css" />  
    <link href="<?php echo base_url();?>themes/scripts/icons/social/stylesheets/social_foundicons.css" media="screen" rel="stylesheet" type="text/css" />
    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    <link rel="stylesheet" href="<?php echo base_url();?>themes/scripts/fontawesome/css/font-awesome.min.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <link href="<?php echo base_url();?>themes/scripts/carousel/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>themes/scripts/camera/css/camera.css" rel="stylesheet" type="text/css" />

    <link href="http://fonts.googleapis.com/css?family=Syncopate" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Abel" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Pontano+Sans" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url();?>themes/styles/custom.css" rel="stylesheet" type="text/css" />
</head>
<body id="pageBody">
<div id="divBoxed" class="container">
    
    <div class="transparent-bg" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;z-index: -1;zoom: 1;"></div>

    <div class="divPanel notop nobottom">
            <div class="row-fluid">
                <div class="span12">

                    <div id="divLogo" class="pull-left">
                        <img id="divSiteTitle" src="<?php echo base_url();?>themes/images/logockpni.png" style="width: 200px;height: auto;">
                        <!--<a href="index.html" id="divSiteTitle">Your Name</a><br />
                        <a href="index.html" id="divTagLine">Your Tag Line Here</a>-->
                    </div>

                    <div id="divMenuRight" class="pull-right">
                        <div class="navbar">
                            <button type="button" class="btn btn-navbar-highlight btn-large btn-primary" data-toggle="collapse" data-target=".nav-collapse">
                                NAVIGATION <span class="icon-chevron-down icon-white"></span>
                            </button>
                            <div class="nav-collapse collapse">
                                <ul class="nav nav-pills ddmenu">
                                    <?php
                                        $sql1=  mysql_query("select * from menu where menu_parent_id='0' and menu_status='Y' order by menu_position asc");
                                        while($data1=  mysql_fetch_array($sql1)){
                                            $sql_sub1=mysql_query("select * from menu where menu_parent_id='".$data1['id']."'");
                                            if(mysql_num_rows($sql_sub1) > 0){ 
                                                if($data1['menu_type']=="4"){
                                                    $menu1=$data1['menu_link'];
                                                }else{
                                                    $menu1=base_url().$data1['menu_link'];
                                                }
                                    ?>
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" href="<?=$menu1;?>" target="<?php echo menu_open($data1['menu_open']);?>"><?=$data1['menu_name'];?> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <?php
                                            $sql2=  mysql_query("select * from menu where menu_parent_id='".$data1['id']."' and menu_status='Y' order by menu_position asc");
                                            while($data2=  mysql_fetch_array($sql2)){ 
                                                $sql_sub2=mysql_query("select * from menu where menu_parent_id='".$data2['id']."'");
                                                if($data2['menu_type']=="4"){
                                                    $menu2=$data2['menu_link'];
                                                }else{
                                                    $menu2=base_url().$data2['menu_link'];
                                                }
                                                if(mysql_num_rows($sql_sub2) > 0){
                                                    
                                            ?>
                                            <li class="dropdown">
                                                <a href="<?=$menu2;?>" target="<?=menu_open($data2['menu_open']);?>" class="dropdown-toggle"><?=$data2['menu_name'];?> &nbsp;&raquo;</a>
                                                <ul class="dropdown-menu sub-menu">
                                                <?php 
                                                $sql3=  mysql_query("select * from menu where menu_parent_id='".$data2['id']."' and menu_status='Y' order by menu_position asc");
                                                        while($data3=  mysql_fetch_array($sql3)){ 
                                                            if($data3['menu_type']=="4"){
                                                                $menu3=$data3['menu_link'];
                                                            }else{
                                                                $menu3=base_url().$data3['menu_link'];
                                                            }
                                                ?>
                                                    <li><a href="<?=$menu3;?>"  target="<?=menu_open($data3['menu_open']);?>"><?=$data3['menu_name'];?></a></li>
                                                <?php } ?>
                                                </ul>
                                            </li>
                                            <?php }else{ ?>
                                            <li><a href="<?=$menu2;?>"  target="<?=menu_open($data2['menu_open']);?>"><?=$data2['menu_name'];?></a></li>
                                            <?php }} ?>
                                        </ul>
                                    </li>
                                    <?php 
                                        }else{
                                            if($data1['menu_type']=="4"){
                                                $menu1=$data1['menu_link'];
                                            }else{
                                                $menu1=base_url().$data1['menu_link'];
                                            }
                                    ?>
                                    <li><a href="<?php echo $menu1;?>" target="<?=menu_open($data1['menu_open']);?>"><?=$data1['menu_name'];?></a></li>
                                    <?php }}?>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            
    </div>
    <div class="clearfix"></div>
    <div class="content">
        <div class="" style="background-color: #E4322D;height: 5px;">
        </div>
    </div>

    <div class="contentArea">

        <div class="divPanel notop page-content">
            
            <?php echo $this->load->view($view);?>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>

    <div id="footerOuterSeparator"></div>

    <div id="divFooter" class="footerArea">

        <div class="divPanel">

            <div class="row-fluid">
                <div class="span3" id="footerArea1">
                
                    <h3>Other Menu</h3>
                    <p> 
                        <?php 
                            $menu_other=$this->db->query("select * from menu_other where menu_parent_id='0' and menu_status='Y' order by menu_position asc")->result();
                            foreach($menu_other as $data_menu_other){
                                if($data_menu_other->menu_type=="4"){
                                    $menu_other_x=$data_menu_other->menu_link;
                                }else{
                                    $menu_other_x=base_url().$data_menu_other->menu_link;
                                }
                        ?>
                        <a href="<?php echo $menu_other_x;?>" title="<?php echo $data_menu_other->menu_name;?>" target="<?php echo menu_open($data_menu_other->menu_open);?>"><?php echo $data_menu_other->menu_name;?></a><br />
                        <?php } ?>
                    </p>

                </div>
                <div class="span3" id="footerArea2">
                    <h3>Recent Blog Posts</h3> 
                    <?php 
                        $recent_blog=$this->db->query("select * from article where status='Y' order by id desc limit 3")->result();
                        foreach($recent_blog as $data_recent_blog){
                    ?>
                    <p>
                        <a href="<?php echo base_url();?>article/index/<?php echo $data_recent_blog->id;?>/<?php echo url($data_recent_blog->article_name);?>" title=""><?php echo $data_recent_blog->article_name;?></a><br />
                        <span style="text-transform:none;">2 hours ago</span>
                    </p>
                        <?php } ?>
                </div>
                <div class="span3" id="footerArea3">

                    <h3>Contact Us</h3>  
                    <?php $data_contact_home=$this->db->query("select * from contact")->row();?>                                           
                    <ul id="contact-info">
                    <li>                                    
                        <i class="general foundicon-phone icon"></i>
                        <span class="field">Phone:</span>
                        <br />
                        <?php echo $data_contact_home->telephone;?>                                                                      
                    </li>
                    <li>
                        <i class="general foundicon-mail icon"></i>
                        <span class="field">Email:</span>
                        <br />
                        <a href="mailto:info@yourdomain.com" title="Email"><?php echo $data_contact_home->email;?></a>
                    </li>
                    <li>
                        <i class="general foundicon-home icon" style="margin-bottom:50px"></i>
                        <span class="field">Address:</span>
                        <br />
                        <?php echo $data_contact_home->address;?>
                    </li>
                    </ul>

                </div
                
                <!--<div class="span3" id="footerArea4">

                    <h3>Follow Us</h3> 
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s. 
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s.
                    </p>

                </div>-->
            </div>

            <br /><br />
            <div class="row-fluid">
                <div class="span12">
                    <p class="copyright">
                        Copyright © 2015 Your LSP-CKPNI. All Rights Reserved.
                    </p>

                    <p class="social_bookmarks">
                        <a href="#"><i class="social foundicon-facebook"></i> Facebook</a>
			<a href=""><i class="social foundicon-twitter"></i> Twitter</a>
			<a href="#"><i class="social foundicon-pinterest"></i> Pinterest</a>
			<a href="#"><i class="social foundicon-rss"></i> Rss</a>
                    </p>
                </div>
            </div>

        </div>
    </div>
</div>
<br /><br /><br />

<script src="<?php echo base_url();?>themes/scripts/jquery.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>themes/scripts/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>themes/scripts/default.js" type="text/javascript"></script>


<script src="<?php echo base_url();?>themes/scripts/carousel/jquery.carouFredSel-6.2.0-packed.js" type="text/javascript"></script><script type="text/javascript">$('#list_photos').carouFredSel({ responsive: true, width: '100%', scroll: 2, items: {width: 320,visible: {min: 2, max: 6}} });</script><script src="<?php echo base_url();?>themes/scripts/camera/scripts/camera.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>themes/scripts/easing/jquery.easing.1.3.js" type="text/javascript"></script>
<script type="text/javascript">function startCamera() {$('#camera_wrap').camera({ fx: 'scrollLeft', time: 2000, loader: 'none', playPause: false, navigation: true, height: '35%', pagination: true });}$(function(){startCamera()});</script>


</body>
</html>