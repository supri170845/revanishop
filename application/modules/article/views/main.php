<div class="breadcrumbs">
    <a href="<?php echo base_url();?>">Home</a> / 
    <span style="text-transform: capitalize;"> Article </span> / 
    <span style="text-transform: capitalize;"><?=$detail->article_name;?></span>
</div>
<div class="row-fluid">
    <!--Edit Sidebar Content here-->
    <div class="span3 sidebar">  
        <div class="sidebox">
            <h4 class="sidebox-title">Reference Article</h4>
            <ul>
                <?php foreach($list_article as $data_list_article){?>
                <li><a href="<?php echo base_url();?>article/index/<?php echo $data_list_article->id;?>/<?php echo url($data_list_article->article_name);?>"><?php echo $data_list_article->article_name;?></a></li>
                <?php } ?>
            </ul>
            <h4 class="sidebox-title">Categories Article</h4>
            <ul>
                <?php foreach($list_category as $data_list_category){?>
                  <li><a href="<?php echo base_url();?>category_article/index/<?php echo $data_list_category->id;?>/<?php echo url($data_list_category->article_category_name);?>"><?php echo $data_list_category->article_category_name;?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <!--/End Sidebar Content -->    
    <!--Edit Main Content Area here-->
    <div class="span9" id="divMain" style="text-align: justify;">
        <h3><?=$detail->article_name;?></h3>
        <hr>
        <?=$detail->article_description;?>                        
    </div>
</div>