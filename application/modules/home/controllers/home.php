<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller{
    public function __construct() {
        parent::__construct();
        
        $this->load->model('main_model');
    }
        
    function index(){
        $data['latest_article']=$this->db->query("select * from article where status='Y' and id_category='4'
                                                  order by id desc
                                                  limit 4
                                                  ")->result();
        $data['slider']=$this->db->query("select * from slider where status='Y'")->result();
        $data['index_article']=$this->db->query("select article_name,article_description from article where index_article='1'")->row();
        $data['view']="main";
        $this->load->view('template',$data);
    }
}