<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class faq extends MX_Controller{
    public function __construct() {
        parent::__construct();
        
        $this->load->model('main_model');
    }
        
    function index(){
        $data['faq']=$this->db->query("select * from faq where status='Y' order by position asc")->result();
        $data['list_category']=$this->db->query("select * from article_category where status='Y'")->result();
        $data['list_article']=$this->db->query("select * from article where status='Y' ORDER BY RAND() limit 6")->result();
        $data['view']="main";
        $this->load->view('template',$data);
    }
}