<div class="breadcrumbs">
    <a href="<?php echo base_url();?>">Home</a> / 
    <span style="text-transform: capitalize;"> FAQ </span> / 
</div>
<div class="row-fluid">
    <!--Edit Sidebar Content here-->
    <div class="span3 sidebar">  
        <div class="sidebox">
            <h4 class="sidebox-title">Reference Article</h4>
            <ul>
                <?php foreach($list_article as $data_list_article){?>
                <li><a href="<?php echo base_url();?>article/index/<?php echo $data_list_article->id;?>/<?php echo url($data_list_article->article_name);?>"><?php echo $data_list_article->article_name;?></a></li>
                <?php } ?>
            </ul>
            <h4 class="sidebox-title">Categories Article</h4>
            <ul>
                <?php foreach($list_category as $data_list_category){?>
                  <li><a href="<?php echo base_url();?>category_article/index/<?php echo $data_list_category->id;?>/<?php echo url($data_list_category->article_category_name);?>"><?php echo $data_list_category->article_category_name;?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <!--/End Sidebar Content -->        							                 
    <!--Edit Main Content Area here-->
    <div class="span9" id="divMain">
        <h1>FAQ</h1>
        <hr>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php foreach($faq as $data_faq){?>
            <div class="panel panel-danger">
                <div class="panel-heading" role="tab" id="heading<?php echo $data_faq->id;?>">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $data_faq->id;?>" aria-expanded="true" aria-controls="collapse<?php echo $data_faq->id;?>">
                          <?php echo $data_faq->question;?>
                        </a>
                    </h4>
                </div>
              <div id="collapse<?php echo $data_faq->id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $data_faq->id;?>">
                    <div class="panel-body" style="text-align: justify;">
                        <?php echo $data_faq->answered;?>
                    </div>
              </div>
            </div>
        <?php } ?>
        </div>
    </div>	                             
</div>